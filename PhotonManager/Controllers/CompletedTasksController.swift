//
//  CompletedTasksController.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 6/29/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import SwipeCellKit
import RealmSwift

/// Class representing the CompletedTasksController
class CompletedTasksController: UIViewController {
    
    // MARK: - Properties
    
    private var notificationToken: NotificationToken?
    var didUpdateConstraints = false
    var completedTasks: Results<Task> = Database.sharedInstance.getCompletedTasks()
    
    // MARK: - Subviews
    
    unowned var completedTasksView: CompletedTasksView { return self.view as! CompletedTasksView }
    unowned var tableView: UITableView { return completedTasksView.tableView }
    
    // MARK: - UIViewController
    
    override func loadView() {
        self.view = CompletedTasksView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(TaskCell.self, forCellReuseIdentifier: "Cell")
        
        // Setup Reactive Table View reload data
        notificationToken = completedTasks.observe { [weak self] (changes: RealmCollectionChange) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                // Results are now populated and can be accessed without blocking the UI
                tableView.reloadData()
            case .update(_, _, _, _):
                // Query results have changed, so apply them to the UITableView
                tableView.reloadDataAnimated(duration: 0.25)
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(error)")
            }
        }
        
        view.setNeedsUpdateConstraints()
        navigationItem.title = "Completed Tasks"
        // Do any additional setup after loading the view.
    }
    
    deinit {
        // Clear the Realm notification observer
        notificationToken?.invalidate()
    }
    
    // MARK: - Setup Functions
    
    func setupNavigationBar() {
        navigationItem.title = "Completed Tasks"
    }
}

// MARK: - Extensions

extension CompletedTasksController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return completedTasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        guard let taskCell = cell as? TaskCell else {return cell}
        taskCell.delegate = self
        taskCell.setupCellFromTask(task: completedTasks[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let task = completedTasks[indexPath.row]
        self.navigationController?.pushViewController(TaskViewController(task: task), animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension CompletedTasksController: SwipeTableViewCellDelegate {
    
    private func processDelete(task: Task) {
        Database.sharedInstance.deleteTask(task: task)
    }
    
    private func processRestart(timeInterval: TimeInterval, task: Task) {
        let dueDate = Date().addingTimeInterval(timeInterval)
        Database.sharedInstance.updateTaskStatus(task: task,
                                                 isActive: true,
                                                 isCompleted: false,
                                                 dueDateInterval: dueDate.timeIntervalSince1970,
                                                 timestamp: Date().timeIntervalSince1970)
        NotificationsManager.sharedInstance.setupNotificationForTask(task: task)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        let task = self.completedTasks[indexPath.row]
        
        if orientation == .left {
            
            let deleteAction = SwipeAction(style: .default, title: "Delete") { _, _ in
                // handle action by updating model with deletion
                self.processDelete(task: task)
                tableView.reloadDataAnimated(duration: 0.25)
            }
            deleteAction.backgroundColor = .red
            
            return [deleteAction]
            
        } else {
            
            let restartAction = SwipeAction(style: .default, title: "Restart") { _, _ in
                CountDownPicker.showPicker(completion: { (interval, _) in
                    guard let interval = interval else {return}
                    self.processRestart(timeInterval: interval, task: task)
                })
            }
            restartAction.hidesWhenSelected = true
            restartAction.backgroundColor = Constants.startSwipeColor
            
            return [restartAction]
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .none
        options.transitionStyle = .border
        return options
    }
}
