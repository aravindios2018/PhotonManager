//
//  ActiveTasksController.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 6/29/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import SwipeCellKit
import RealmSwift

/// Class representing the ActiveTasksController
class ActiveTasksController: UIViewController {
    
    // MARK: - Properties
    
    var notificationToken: NotificationToken?
    var activeTasks: Results<Task> = Database.sharedInstance.getActiveTasks()
    var pendingTasks: Results<Task> = Database.sharedInstance.getPendingTasks()
    var activeAndPendingTasks: Results<Task> = Database.sharedInstance.getActiveAndPendingTasks()
    
    // MARK: - Subviews
    
    unowned var activeTasksView: ActiveTasksView { return self.view as! ActiveTasksView }
    unowned var tableView: UITableView { return activeTasksView.tableView }
    
    // MARK: - UIViewController
    
    override func loadView() {
        self.view = ActiveTasksView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Active Tasks"
        
        activeTasks = Database.sharedInstance.getActiveTasks()
        pendingTasks = Database.sharedInstance.getPendingTasks()
        activeAndPendingTasks = Database.sharedInstance.getActiveAndPendingTasks()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(TaskCell.self, forCellReuseIdentifier: "Cell")
        
        // Setup Reactive Table View reload data
        notificationToken = activeAndPendingTasks.observe { [weak self] (changes: RealmCollectionChange) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                // Results are now populated and can be accessed without blocking the UI
                tableView.reloadData()
            case .update(_, _, _, _):
                // Query results have changed, so apply them to the UITableView
                tableView.reloadDataAnimated(duration: 0.25)
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(error)")
            }
        }
        
        view.setNeedsUpdateConstraints()
        // Do any additional setup after loading the view.
    }
    
    deinit {
        // Clear the Realm notification observer
        notificationToken?.invalidate()
    }
    
    // MARK: - Setup Functions
    
    func setupNavigationBar() {
        navigationItem.title = "Active Tasks"
    }
}

extension ActiveTasksController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getTasksForSection(section: section).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        guard let taskCell = cell as? TaskCell else {return cell}
        taskCell.delegate = self
        taskCell.setupCellFromTask(task: taskForIndexPath(indexPath: indexPath))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let task = taskForIndexPath(indexPath: indexPath)
        self.navigationController?.pushViewController(TaskViewController(task: task), animated: true)
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let activeCnt = self.activeTasks.count
        let pendingCnt = self.pendingTasks.count
        
        if section == 0 {
            return activeCnt == 0 ? 0 : 30
        } else { return pendingCnt == 0 ? 0 : 30 }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Active" : "Pending"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}

extension ActiveTasksController: SwipeTableViewCellDelegate {
    
    private func processPostpone(task: Task) {
        let postponedDate = Date.init(timeIntervalSince1970: task.dueDateInterval).addMinutes(minutes: 1)
        Database.sharedInstance.updateTaskStatus(task: task,
                                                 dueDateInterval: postponedDate.timeIntervalSince1970)
        NotificationsManager.sharedInstance.setupNotificationForTask(task: task)
    }
    
    private func processCancel(task: Task) {
        Database.sharedInstance.updateTaskStatus(task: task,
                                                 isActive: false,
                                                 dueDateInterval: 0)
        NotificationsManager.sharedInstance.removeNotificationForTask(task: task)
    }
    
    private func processStart(timeInterval: TimeInterval, task: Task) {
        let dueDate = Date().addingTimeInterval(timeInterval)
        Database.sharedInstance.updateTaskStatus(task: task,
                                                 isActive: true,
                                                 dueDateInterval: dueDate.timeIntervalSince1970,
                                                 timestamp: Date().timeIntervalSince1970)
        NotificationsManager.sharedInstance.setupNotificationForTask(task: task)
    }
    
    private func processFinish(task: Task) {
        Database.sharedInstance.updateTaskStatus(task: task,
                                                 isActive: false,
                                                 isCompleted: true)
        NotificationsManager.sharedInstance.removeNotificationForTask(task: task)
    }
    
    func getTasksForSection(section: Int) -> Results<Task> {
        return section == 0 ? self.activeTasks : self.pendingTasks
    }
    
    func taskForIndexPath(indexPath: IndexPath) -> Task {
        return getTasksForSection(section: indexPath.section)[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        let task = self.taskForIndexPath(indexPath: indexPath)
        
        if orientation == .left {
            
            let postponeAction = SwipeAction(style: .default, title: "Postpone") { _, _ in self.processPostpone(task: task) }
            postponeAction.backgroundColor = Constants.globalTintColor
            
            let cancelAction = SwipeAction(style: .default, title: "Cancel") { _, _ in self.processCancel(task: task) }
            cancelAction.backgroundColor = .red
            
            return task.isActive ? [cancelAction, postponeAction] : []
            
        } else {
            
            let startAction = SwipeAction(style: .default, title: "Start") { _, _ in
                CountDownPicker.showPicker(completion: { (interval, _) in
                    guard let interval = interval else {return}
                    self.processStart(timeInterval: interval, task: task)
                })
            }
            startAction.hidesWhenSelected = true
            startAction.backgroundColor = Constants.startSwipeColor
            
            let finishAction = SwipeAction(style: .default, title: "Finish") { _, _ in self.processFinish(task: task) }
            finishAction.backgroundColor = Constants.finishSwipeColor
            
            return task.isActive ? [finishAction] : [startAction]
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .none
        options.transitionStyle = .border
        return options
    }
}
