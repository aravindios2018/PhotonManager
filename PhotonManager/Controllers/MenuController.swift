//
//  ViewController.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 6/28/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import SnapKit

/// Class representing main MenuController 
class MenuController: UITabBarController {
    
    // MARK: - Variables
    
    var didUpdateConstraints = false
    
    // MARK: - Subviews
    
    lazy var centralButton: UIButton! = { [unowned self] in
        let customButton = CentralTabButton.init(frame: CGRect.init(x: (Constants.globalScreenSize.width - Constants.centralTabButtonSize.width)/2,
                                                                    y: Constants.globalScreenSize.height - Constants.centralTabButtonSize.height - 10,
                                                                    width: Constants.centralTabButtonSize.width,
                                                                    height: Constants.centralTabButtonSize.height), icon: #imageLiteral(resourceName: "centralIcon"))
        customButton.backgroundColor = UIColor.black
        customButton.imageView?.contentMode = .scaleAspectFit
        customButton.layer.borderWidth = 2
        customButton.layer.borderColor = UIColor.white.cgColor
        customButton.addTarget(self, action: #selector(addTaskBtnPressed), for: .touchUpInside)
        
        return customButton
    }()
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup active tasks screen
        let firstViewController = ActiveTasksController()
        firstViewController.view.backgroundColor = Constants.globalBackgroundColor
        firstViewController.tabBarItem.image = #imageLiteral(resourceName: "tab1")
        firstViewController.tabBarItem.imageInsets = UIEdgeInsets.init(top: 2, left: -15, bottom: -2, right: 15)
        
        // Setup completed tasks screen
        let secondViewController = CompletedTasksController()
        secondViewController.view.backgroundColor = Constants.globalBackgroundColor
        secondViewController.tabBarItem.image = #imageLiteral(resourceName: "tab2")
        secondViewController.tabBarItem.imageInsets = UIEdgeInsets.init(top: 2, left: 15, bottom: -2, right: -15)
        
        let tabBarList = [UINavigationController.init(rootViewController: firstViewController), UINavigationController.init(rootViewController: secondViewController)]
        viewControllers = tabBarList
        
        // Add central tab bar item
        self.view.addSubview(centralButton)
        
        self.view.setNeedsUpdateConstraints()
    }
    
    override func updateViewConstraints() {
        if (!didUpdateConstraints) {
            
            didUpdateConstraints = true
        }
        
        super.updateViewConstraints()
    }
    
    // MARK: - Actions
    
    @objc func addTaskBtnPressed() {
        self.present(UINavigationController.init(rootViewController: TaskViewController()), animated: true, completion: nil)
    }
}
