//
//  TaskCell.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 7/1/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import SwipeCellKit
import CountdownLabel

/// Dynamic task cell class
class TaskCell: SwipeTableViewCell {
    
    // MARK: - Subviews
    
    let accessoryTimerView: CountdownLabel! = {
        let timer = CountdownLabel(frame: CGRect.init(x: 0, y: 0, width: 70, height: 40))
        timer.animationType = .Evaporate
        timer.timeFormat = "HH:mm:ss"
        timer.textColor = .white
        return timer
    }()
    
    let timeElapsedLabel: UILabel! = {
        let label = UILabel(frame: CGRect.init(x: 0, y: 0, width: 70, height: 50))
        label.textColor = .white
        return label
    }()
    
    // MARK: - UIView
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = Constants.globalContentColor
        self.textLabel?.textColor = .white
        self.detailTextLabel?.textColor = .lightGray
        self.appendBottomShadowView(color: Constants.globalShadowColor)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    // MARK: - Actions
    
    private func refreshAccessoryView(taskIsCompleted: Bool) {
        self.accessoryView = taskIsCompleted ? self.timeElapsedLabel : self.accessoryTimerView
    }
    
    /// Sets up dynamic cell according to particular task type
    ///
    /// - Parameter task: Task instance
    func setupCellFromTask(task: Task) {
        self.textLabel?.text = task.name
        self.detailTextLabel?.text = task.taskDescription
        
        let durationDate = Date(timeIntervalSince1970: task.dueDateInterval)
        let currentDate = Date()
        
        if task.isCompleted {
            let stampDate = Date(timeIntervalSince1970: task.timestamp)
            let timeDiff = durationDate.timeIntervalSince(stampDate)
            let roundedDiff = Double(round(100 * timeDiff) / 100) / 60
            self.timeElapsedLabel.text = "\(roundedDiff) min."
        } else {
            if !task.isActive {
                self.accessoryTimerView.cancel()
                self.accessoryTimerView.isHidden = true
            } else if (durationDate < currentDate) {
                Database.sharedInstance.updateTaskStatus(task: task, isActive: false, isCompleted: true)
            } else {
                self.accessoryTimerView.setCountDownDate(targetDate: durationDate as NSDate)
                self.accessoryTimerView.start()
                self.accessoryTimerView.isHidden = false
            }
        }
        
        refreshAccessoryView(taskIsCompleted: task.isCompleted)
    }
    
}
