//
//  CompletedTasksView.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 7/4/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import SnapKit
import DZNEmptyDataSet

/// The completed tasks view class used as main view in CompletedTasksController
class CompletedTasksView: UIView {

    // MARK: - Variables
    
    var didUpdateConstraints = false
    
    // MARK: - Subviews
    
    let tableView: UITableView! = {
        let tableView = UITableView.init(frame: CGRect.zero, style: UITableViewStyle.plain)
        tableView.separatorStyle = .none
        tableView.backgroundColor = Constants.globalBackgroundColor
        tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        
        return tableView
    }()
    
    // MARK: - UIView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(tableView)
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        setNeedsUpdateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        
        if (!didUpdateConstraints) {
            
            self.tableView.snp.makeConstraints { (make) in
                make.left.equalTo(self.snp.left)
                make.top.equalTo(self.snp.top)
                make.right.equalTo(self.snp.right)
                make.bottom.equalTo(self.snp.bottom)
            }
            
            didUpdateConstraints = true
        }
        
        super.updateConstraints()
    }
}

// MARK: - Extensions

extension CompletedTasksView: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString.init(string: "No Completed Tasks")
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString.init(string: "Make sure to finish at least one task to browse data 🙂")
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "emptyDataLogo")
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}
