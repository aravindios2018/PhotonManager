//
//  InputTableCell.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 7/1/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import SnapKit

/// Protocol for encapsulating UITextField modifications
protocol InputTableCellDelegate: class {
    func inputFieldDidChanged()
}

/// Static task cell class with simple UITextField
class InputTableCell: UITableViewCell {
    
    // MARK: - Variables
    
    weak public var delegate: InputTableCellDelegate?
    private var didSetupContraints = false
    var inputText: String? {
        set { inputTextField.text = newValue }
        get { return inputTextField.text }
    }
    var inputPlaceholder: String? {
        set {
            inputTextField.placeholder = newValue
            inputTextField.setPlaceHolderColor(color: UIColor.lightGray)
        }
        get { return inputTextField.placeholder }
    }
    
    // MARK: - Subviews
    
    private var inputTextField: UITextField! = {
        let inputTextField = UITextField()
        inputTextField.textColor = .white
        inputTextField.backgroundColor = .clear
        inputTextField.setPlaceHolderColor(color: UIColor.lightGray)
        return inputTextField
    }()
    
    // MARK: - UITableViewCell
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(inputTextField)
        self.inputTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        self.backgroundColor = Constants.globalContentColor
        
        appendBottomShadowView(color: Constants.globalShadowColor)
        
        self.setNeedsUpdateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        if (!didSetupContraints) {
            
            self.inputTextField.snp.makeConstraints { (make) in
                make.bottom.equalTo(self.snp.bottom)
                make.right.equalTo(self.snp.right).offset(15)
                make.left.equalTo(self.snp.left).inset(15)
                make.top.equalTo(self.snp.top)
            }
            
            didSetupContraints = true
        }
        
        super.updateConstraints()
    }
    
    // MARK: - Textfield observer
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        delegate?.inputFieldDidChanged()
    }
}
