//
//  KeywordsTableCell.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 7/1/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import SnapKit
import RealmSwift
import RKTagsView

/// Static cell class encapsulating custom TagsView
class KeywordsTableCell: UITableViewCell {
    
    // MARK: - Variables
    
    private var didSetupContraints = false
    var inputPlaceholder: String {
        set { tagsTextField.toolbarPlaceholder = newValue }
        get { return tagsTextField.toolbarPlaceholder ?? "" }
    }
    var tagsList: [String] { return tagsTextField.tags }
    
    // MARK: - Subviews
    
    private lazy var tagsTextField: RKTagsView! = {
        var field = RKTagsView()
        
        field.backgroundColor = .clear
        field.textField.textColor = .white
        field.textField.placeholder = "Tap to add keywords"
        field.tintColor = Constants.globalTintColor
        field.textField.setPlaceHolderColor(color: UIColor.lightGray)
        
        return field
    }()
    
    // MARK: - UITableViewCell
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(tagsTextField)
        self.clipsToBounds = true
        self.backgroundColor = Constants.globalContentColor
        appendBottomShadowView(color: Constants.globalShadowColor)
        self.setNeedsUpdateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        if (!didSetupContraints) {
            
            self.tagsTextField.snp.makeConstraints { (make) in
                make.bottom.equalTo(self.snp.bottom).offset(10)
                make.right.equalTo(self.snp.right).inset(15)
                make.left.equalTo(self.snp.left).offset(15)
                make.top.equalTo(self.snp.top).inset(15)
            }
            
            didSetupContraints = true
        }
        
        super.updateConstraints()
    }
    
    // MARK: - Actions
    
    func fillWithTags(rawTags: List<String>) {
        rawTags.forEach { (tag) in
            self.tagsTextField.addTag(tag)
        }
        //        self.tagsTextField.addta .addTags(Array(rawTags))
    }
}
