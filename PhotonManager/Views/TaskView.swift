//
//  TaskView.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 7/4/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import UIKit

/// The task view class used as main view in TaskViewController
class TaskView: UIView {

    // MARK: - Variables
    
    var state: TaskControllerState = .add
    private var didUpdateConstraints = false
    
    // MARK: - Subviews
    
    let tableView: UITableView! = {
        let tableView = UITableView.init(frame: CGRect.zero, style: .grouped)
        tableView.separatorStyle = .none
        tableView.backgroundColor = Constants.globalBackgroundColor
        tableView.tableFooterView = UIView.init(frame: CGRect.zero)
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.contentInset = UIEdgeInsets.init(top: -20, left: 0, bottom: 0, right: 0)
        
        return tableView
    }()
    
    let nameInputCell: InputTableCell! = {
        let nameInputCell = InputTableCell()
        nameInputCell.inputPlaceholder = "Name"
        return nameInputCell
    }()
  
    let descriptionCell: InputTableCell! = {
        let descriptionCell = InputTableCell()
        descriptionCell.inputPlaceholder = "Description"
        return descriptionCell
    }()
 
    lazy var keywordsCell: KeywordsTableCell! = {
        let keywordsCell = KeywordsTableCell()
        keywordsCell.inputPlaceholder = "Add keyword"
        return keywordsCell
    }()
    
    lazy var attachFileButton: UIButton! = {
        var button = UIButton()
        button.setTitle("Attach File", for: UIControlState.normal)
        button.backgroundColor = Constants.globalTintColor
        button.setTitleColor(Constants.globalContentColor, for: UIControlState.normal)
        button.setTitleColor(UIColor.white, for: UIControlState.highlighted)
        return button
    }()
    
    // MARK: - UIView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(tableView)
        if (self.state == .add) {
            self.addSubview(attachFileButton)
        }
        
        setNeedsUpdateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        
        if (!didUpdateConstraints) {
            
            if self.state == .edit {
                self.tableView.snp.makeConstraints { (make) in
                    make.left.equalTo(self.snp.left)
                    make.top.equalTo(self.snp.top)
                    make.right.equalTo(self.snp.right)
                    make.bottom.equalTo(self.snp.bottom)
                }
            } else {
                self.attachFileButton.snp.makeConstraints { (make) in
                    make.bottom.equalTo(self.snp.bottom)
                    make.left.equalTo(self.snp.left)
                    make.right.equalTo(self.snp.right)
                    make.height.equalTo(44)
                }
                
                self.tableView.snp.makeConstraints { (make) in
                    make.left.equalTo(self.snp.left)
                    make.top.equalTo(self.snp.top)
                    make.right.equalTo(self.snp.right)
                    make.bottom.equalTo(self.attachFileButton.snp.top)
                }
            }
            
            didUpdateConstraints = true
        }
        
        super.updateConstraints()
    }
}
