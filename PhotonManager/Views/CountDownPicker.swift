//
//  CountDownPicker.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 7/4/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import SnapKit

/// A protocol for delegating the actions performed on picker
protocol CountDownPickerDelegate: class {
    func didSelectDate(timeIntervalInSeconds: TimeInterval)
    func didCloseDatePopup(status: Bool)
}

/// A custom view that is used for selecting the duration for tasks
class CountDownPicker: UIView {
    
    // MARK: - Variables
    
    public weak var delegate: CountDownPickerDelegate?
    private var didUpdateConstraints = false
    public typealias CompletionHandler = (TimeInterval?, Bool) -> Void
    private var action: CompletionHandler?
    
    // MARK: - Subviews
    
    private let titleLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    private lazy var cancelButton: UIButton = { [unowned self] in
        var button = UIButton()
        button.setTitle("Cancel", for: .normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.setTitleColor(.red, for: .normal)
        button.addTarget(self, action: #selector(didTapCancel), for: .touchUpInside)
        return button
        }()
    
    private lazy var doneButton: UIButton = { [unowned self] in
        var button = UIButton()
        button.setTitle("Done", for: .normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.addTarget(self, action: #selector(didTapDone), for: .touchUpInside)
        return button
        }()
    
    private let datePicker: UIDatePicker = {
        var picker = UIDatePicker()
        picker.datePickerMode = .countDownTimer
        picker.minuteInterval = 1
        picker.timeZone = TimeZone.current
        
        return picker
    }()
    
    override var tintColor: UIColor! {
        didSet {
            self.titleLabel.tintColor = tintColor
            self.cancelButton.tintColor = tintColor
            self.doneButton.tintColor = tintColor
            self.datePicker.tintColor = tintColor
        }
    }
    
    var headerTextColor: UIColor! {
        didSet {
            self.titleLabel.textColor = headerTextColor
            self.doneButton.setTitleColor(headerTextColor, for: .normal)
        }
    }
    
    var pickerTextColor: UIColor = .white {
        didSet {
            self.datePicker.setValue(pickerTextColor, forKey: "textColor")
        }
    }
    
    var titleText: String = "" {
        didSet {
            self.titleLabel.text = titleText
        }
    }
    
    // MARK: - UIView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        let panRecognizer = UIPanGestureRecognizer.init(target: self, action: #selector(panPiece(gestureRecognizer:)))
        self.addGestureRecognizer(panRecognizer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        if (!didUpdateConstraints) {
            
            self.titleLabel.snp.makeConstraints { (make) in
                make.left.equalTo(self.snp.left)
                make.right.equalTo(self.snp.right)
                make.top.equalTo(self.snp.top)
                make.height.equalTo(40)
            }
            
            self.cancelButton.snp.makeConstraints { (make) in
                make.right.equalTo(self.snp.centerX)
                make.bottom.equalTo(self.snp.bottom)
                make.left.equalTo(self.snp.left)
                make.height.equalTo(40)
            }
            
            self.doneButton.snp.makeConstraints { (make) in
                make.right.equalTo(self.snp.right)
                make.bottom.equalTo(self.snp.bottom)
                make.left.equalTo(self.snp.centerX)
                make.height.equalTo(40)
            }
            
            self.datePicker.snp.makeConstraints { (make) in
                make.top.equalTo(self.titleLabel.snp.bottom)
                make.left.equalToSuperview()
                make.right.equalToSuperview()
                make.bottom.equalTo(self.doneButton.snp.top)
            }
            
            didUpdateConstraints = true
        }
        
        super.updateConstraints()
    }
    
    // MARK: - Setup Functions
    
    func setupViews() {
        addSubview(doneButton)
        addSubview(cancelButton)
        addSubview(datePicker)
        addSubview(titleLabel)
        setNeedsUpdateConstraints()
        
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
    }
    
    // MARK: - Actions
    
    static func showPicker(completion: @escaping CompletionHandler) {
        let screenSize = Constants.globalScreenSize
        let size = Constants.durationPickerSize
        
        let picker = CountDownPicker.init(frame: CGRect.init(x: (screenSize.width - size.width)/2,
                                                             y: (screenSize.height - size.height)/2,
                                                             width: size.width,
                                                             height: size.height))
        picker.tintColor = .white
        picker.backgroundColor = Constants.globalShadowColor
        picker.titleText = "Specify Duration"
        picker.headerTextColor = Constants.globalTintColor
        picker.pickerTextColor = .white
        picker.present(completion: completion)
    }
    
    func present(completion: @escaping CompletionHandler) {
        self.action = completion
        Constants.globalWindow?.addSubview(self)
    }
    
    @objc private func didTapCancel() {
        if let action = self.action {
            action(nil, false)
        }
        
        self.delegate?.didCloseDatePopup(status: false)
        self.closePopup()
    }
    
    @objc private func didTapDone() {
        
        let interval = self.datePicker.countDownDuration
        
        if let action = self.action {
            action(interval, true)
        }
        
        self.delegate?.didSelectDate(timeIntervalInSeconds: interval)
        self.delegate?.didCloseDatePopup(status: true)
        self.closePopup()
    }
    
    private func closePopup() {
        UIView.animate(withDuration: 0.25, animations: {
            self.transform = self.transform.scaledBy(x: 0.1, y: 0.1)
            self.alpha = 0
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
    // MARK: - Pan Gestures
    // Allows dragging and dismissing the view
    
    private var originalPoint: CGPoint = CGPoint.zero
    
    @objc private func panPiece(gestureRecognizer: UIPanGestureRecognizer) {
        
        let xDistance = gestureRecognizer.translation(in: self).x
        let yDistance = gestureRecognizer.translation(in: self).y
        
        switch gestureRecognizer.state {
        case .began:
            self.originalPoint = self.center
        case .changed:
            let rotationStrength = min(xDistance / 320, 1)
            let rotationAngel = (CGFloat) (2*CGFloat(Double.pi) * rotationStrength / 16)
            let scaleStrength = 1 - fabs(rotationStrength) / 4
            let scale = max(scaleStrength, 0.93)
            self.center = CGPoint.init(x: self.originalPoint.x + xDistance, y: self.originalPoint.y + yDistance)
            let rotationTransform = CGAffineTransform.init(rotationAngle: rotationAngel)
            let scaleTransform = rotationTransform.scaledBy(x: scale, y: scale)
            self.transform = scaleTransform
        case .ended:
            
            if (fabs(xDistance)/UIScreen.main.bounds.width > 0.4 || fabs(yDistance)/UIScreen.main.bounds.height > 0.4) {
                self.closePopup()
            } else { self.resetViewPositionAndTransformations() }
            
        default:
            break
        }
    }
    
    private func resetViewPositionAndTransformations() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 2, options: .curveEaseInOut, animations: {
            self.center = self.originalPoint
            self.transform = CGAffineTransform.init(rotationAngle: 0)
        }, completion: nil)
    }
}
