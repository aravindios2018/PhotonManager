//
//  PopButton.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 7/4/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import UIKit

/// Simple custom UIButton class used for central button in tabBar
class CentralTabButton: UIButton {
    
    // MARK: - UIView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    convenience init(frame: CGRect, icon: UIImage) {
        self.init(frame: frame)
        self.setImage(icon, for: .normal)
    }
    
    func setupView() {
        self.clipsToBounds = true
        self.imageView?.contentMode = .scaleAspectFit
        self.layer.cornerRadius = Constants.centralTabButtonSize.width / 2
        self.layer.shadowColor = Constants.globalShadowColor.cgColor
        self.layer.shadowOpacity = 0.8
        self.layer.shadowRadius = 5.0
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        self.tintColor = .white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        UIView.animate(withDuration: 0.35, delay: 0.0, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform.init(scaleX: 1.1, y: 1.1)
        }, completion: { (_) in
            
        })
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            super.touchesEnded(touches, with: event)
        }
        
        super.touchesEnded(touches, with: event)
        UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
        }, completion: { (status) in
            UIView.animate(withDuration: 0.25, delay: 0.0, options: .curveEaseInOut, animations: {
                self.transform = CGAffineTransform.init(scaleX: 1.0, y: 1.0)
            }, completion: { (status) in
                if status {
                }
            })
        })
    }
}
