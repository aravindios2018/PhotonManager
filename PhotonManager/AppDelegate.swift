//
//  AppDelegate.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 6/28/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import UserNotifications
import RevealingSplashView

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
    
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.tintColor = Constants.globalTintColor
    
        // Setup main menu controller with Splash animation on first launch
        let menuVC = MenuController()
        let revealingSplashView = RevealingSplashView(iconImage: #imageLiteral(resourceName: "launchLogo"),
                                                      iconInitialSize: CGSize(width: 150, height: 150),
                                                      backgroundColor: Constants.globalContentColor)
        menuVC.view.addSubview(revealingSplashView)
        
        window?.rootViewController = menuVC
        window?.makeKeyAndVisible()
        
        //Starts animation
        revealingSplashView.startAnimation()
        
        // Setup singleton calls
        IQKeyboardManager.sharedManager().enable = true
        NotificationsManager.sharedInstance.authorizeNotifications()
        
        // Setup global colors
        AppDelegate.setupColorPallete()
        
        return true
    }
    
    private static func setupColorPallete() {
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.isTranslucent = false
        navigationBarAppearace.barStyle = .blackTranslucent
        navigationBarAppearace.barTintColor = Constants.globalContentColor
        navigationBarAppearace.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        UIPickerView.appearance().backgroundColor = Constants.globalContentColor
        UITextField.appearance().keyboardAppearance = .dark
        UIApplication.shared.statusBarStyle = .lightContent
        UITabBar.appearance().barTintColor = Constants.globalContentColor
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}
