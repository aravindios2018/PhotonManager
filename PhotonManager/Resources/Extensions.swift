//
//  Extensions.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 6/30/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import SnapKit
//import WSTagsField
import RKTagsView
import RealmSwift

extension UIColor {
    
    public convenience init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat = 1) {
        self.init(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: a)
    }
    
    public convenience init?(hexString: String, alpha: CGFloat = 1.0) {
        var formatted = hexString.replacingOccurrences(of: "0x", with: "")
        formatted = formatted.replacingOccurrences(of: "#", with: "")
        if let hex = Int(formatted, radix: 16) {
            let red = CGFloat(CGFloat((hex & 0xFF0000) >> 16)/255.0)
            let green = CGFloat(CGFloat((hex & 0x00FF00) >> 8)/255.0)
            let blue = CGFloat(CGFloat((hex & 0x0000FF) >> 0)/255.0)
            self.init(red: red, green: green, blue: blue, alpha: alpha)        } else {
            return nil
        }
    }
}

extension Date {
    
    public func addMinutes(minutes: Int) -> Date {
        return self.addingTimeInterval(Double(minutes) * 60)
    }
}

extension UIViewController {    
    public func setRightNavigationBarStatus(enabled: Bool) {
        navigationItem.rightBarButtonItem?.isEnabled = enabled
    }
}

extension UITableView {
    public func reloadDataAnimated(duration: Double) {
        UIView.transition(with: self,
                          duration: duration,
                          options: .transitionCrossDissolve,
                          animations: { self.reloadData() })
    }
    
    func applyChanges<T>(changes: RealmCollectionChange<T>) {
        switch changes {
        case .initial: reloadData()
        case .update(_, let deletions, let insertions, let updates):
            let fromRow = {(row: Int) in
                return IndexPath(row: row, section: 0)}
            
            beginUpdates()
            deleteRows(at: deletions.map(fromRow), with: .automatic)
            insertRows(at: insertions.map(fromRow), with: .automatic)
            reloadRows(at: updates.map(fromRow), with: .none)
            endUpdates()
        default: break
        }
    }
}

extension UITextField {
    func setPlaceHolderColor(color: UIColor) {
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedStringKey.foregroundColor: color])
    }
}

extension UIView {
    
    /// Appends a tiny UIView with shadow color to the bottom of current view
    ///
    /// - Parameter color: Required color
    public func appendBottomShadowView(color: UIColor) {
        let shadowView = UIView()
        shadowView.backgroundColor = color
        addSubview(shadowView)
        
        shadowView.snp.makeConstraints { (make) in
            make.height.equalTo(2)
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.bottom.equalTo(self.snp.bottom)
        }
    }
}
