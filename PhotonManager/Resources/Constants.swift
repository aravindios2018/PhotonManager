//
//  Constants.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 6/30/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import RealmSwift

class Constants {
    
    // MARK: - Colors
    static let globalBackgroundColor = UIColor.init(hexString: "0x131212")!
    static let globalContentColor = UIColor.init(hexString: "0x232020")!
    static let globalShadowColor = UIColor.init(hexString: "0x333030")!
    static let globalTintColor = UIColor.init(hexString: "0xFFC000")!
    static let startSwipeColor = UIColor.init(hexString: "0x007B79")!
    static let finishSwipeColor = UIColor.init(hexString: "0x4E8098")!
    
    // MARK: - Custom sizes
    
    static let centralTabButtonSize = CGSize.init(width: 55, height: 55)
    static let durationPickerSize = CGSize.init(width: UIScreen.main.bounds.width - 80, height: UIScreen.main.bounds.height * 0.5)
    static let globalScreenSize = UIScreen.main.bounds.size
    
    // MARK: - Custom Accessors
    static let globalWindow = (UIApplication.shared.delegate as! AppDelegate).window
}
