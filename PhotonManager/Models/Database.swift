//
//  Database.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 7/1/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import Foundation
import RealmSwift

/// The database model encapsulating Realm
class Database {
    
    // MARK: - Variables
    
    private var database: Realm
    static let sharedInstance = Database()
    
    // MARK: - Constructor
    
    private init() {
        database = try! Realm()
    }
    
    // MARK: - Functions

    /// A method to return all active tasks in database
    ///
    /// - Returns: All active tasks in database
    func getActiveTasks() -> Results<Task> {
        return database.objects(Task.self).filter("isActive == true AND isCompleted == false")
    }
    
    /// A method to return all pending tasks in database
    ///
    /// - Returns: All pending tasks in database
    func getPendingTasks() -> Results<Task> {
        return database.objects(Task.self).filter("isActive == false AND isCompleted == false")
    }
    
    /// A method to return all active and pending tasks in database
    ///
    /// - Returns: All active and pending tasks in database
    func getActiveAndPendingTasks() -> Results<Task> {
        return database.objects(Task.self).filter("isCompleted == false")
    }
    
    /// A method to return all completed tasks in database
    ///
    /// - Returns: All completed tasks in database
    func getCompletedTasks() -> Results<Task> {
        return database.objects(Task.self).filter("isCompleted == true")
    }
    
    /// Return specific task by ID
    ///
    /// - Parameter taskID: Unique ID of the task
    /// - Returns: Realm object task instance
    func getTaskById(taskID: String) -> Task? {
        return database.object(ofType: Task.self, forPrimaryKey: taskID)
    }
    
    /// Updates various fields of task instance by firstly retreiving task by ID
    ///
    /// - Parameters:
    ///   - taskID: Unique task ID
    ///   - isActive: Identifies whether task is active
    ///   - isCompleted: Identifies whether task is completed
    ///   - dueDateInterval: Interval at which task needs to be finished
    ///   - timestamp: Interval at which task was marked as active
    func updateTaskStatusById(taskID: String, isActive: Bool? = nil, isCompleted: Bool? = nil, dueDateInterval: TimeInterval? = nil,
                              timestamp: TimeInterval? = nil) {
        guard let task = self.getTaskById(taskID: taskID) else { return }
        self.updateTaskStatus(task: task,
                              isActive: isActive,
                              isCompleted: isCompleted,
                              dueDateInterval: dueDateInterval,
                              timestamp: timestamp)
    }
    
    /// Updates various fields of task instance
    ///
    /// - Parameters:
    ///   - task: Task instance
    ///   - isActive: Identifies whether task is active
    ///   - isCompleted: Identifies whether task is completed
    ///   - dueDateInterval: Interval at which task needs to be finished
    ///   - timestamp: Interval at which task was marked as active
    ///   - name: Name of the task
    ///   - description: Description of the task
    ///   - tags: String array of tag keywords
    func updateTaskStatus(task: Task, isActive: Bool? = nil, isCompleted: Bool? = nil, dueDateInterval: TimeInterval? = nil,
                          timestamp: TimeInterval? = nil, name: String? = nil, description: String? = nil,
                          tags: [String]? = nil) {
        try! database.write {
            if let active = isActive { task.isActive = active }
            if let completed = isCompleted { task.isCompleted = completed }
            if let dueDate = dueDateInterval { task.dueDateInterval = dueDate }
            if let stamp = timestamp {task.timestamp = stamp}
            if let name = name {task.name = name}
            if let description = description {task.taskDescription = description}
            if let tags = tags {
                task.tags.removeAll()
                task.tags.append(objectsIn: tags)
            }
        }
    }
    
    /// Adds task into database
    ///
    /// - Parameter task: Task instance
    func addTask(task: Task) {
        try! database.write {
            database.add(task, update: true)
            print("Added new object")
        }
    }
    
    /// Removes task from database
    ///
    /// - Parameter task: Task instance
    func deleteTask(task: Task) {
        try! database.write {
            database.delete(task)
        }
    }
}
