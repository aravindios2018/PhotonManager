//
//  TimeManager.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 7/2/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import Foundation
import UserNotifications
import RealmSwift

/// NotificationManager class that performs all local notifcation related logic
class NotificationsManager: NSObject, UNUserNotificationCenterDelegate {
    
    // MARK: - Variables
    
    static let sharedInstance = NotificationsManager()
    let notificationCenter = UNUserNotificationCenter.current()
    private var notificationsAllowed = false
    
    // MARK: - Functions
    
    /// Asks user to allow app sending notifications
    func authorizeNotifications() {
        notificationCenter.requestAuthorization(options: [.alert, .sound, .badge]) { [unowned self] (_, error) in
            //handle result of request failure
            if error == nil {
                self.notificationsAllowed = true
                self.notificationCenter.delegate = self
                self.removeFinishedNotificationsAndTasks()
            }
        }
    }
    
    /// Removes notification assigned for particular task ID
    ///
    /// - Parameter task: Task instance
    func removeNotificationForTask(task: Task) {
        notificationCenter.removePendingNotificationRequests(withIdentifiers: [task.taskID])
    }
    
    /// Sets new notification for specific duration for task instance
    ///
    /// - Parameter task: Task instance
    func setupNotificationForTask(task: Task) {
        
        if !notificationsAllowed {
            return
        }
        
        self.removeNotificationForTask(task: task)
        
        let content = UNMutableNotificationContent()
        content.title = "Task `\(task.name)` Completed!"
        content.body = "Explore the `Completed` section to check the completed task."
        content.sound = UNNotificationSound.default()
        content.userInfo = ["TaskID": task.taskID]
        
        let triggerDate = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: Date.init(timeIntervalSince1970: task.dueDateInterval))
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
        
        let request = UNNotificationRequest(identifier: task.taskID, content: content, trigger: trigger)
        
        //add request to notification center
        notificationCenter.add(request) { (error) in
            if error != nil {
                print("error \(String(describing: error))")
            }
        }
    }
    
    /// Removes all finished notifications
    func removeFinishedNotificationsAndTasks() {
        self.notificationCenter.removeAllDeliveredNotifications()
    }
    
    /// Marks task obtained by task id in provided notification as completed
    ///
    /// - Parameter notification: An instance of UNNotification
    private func completeTaskInNotification(notification: UNNotification) {
        if let taskId = notification.request.content.userInfo["TaskID"] as? String {
            DispatchQueue.main.async {
                Database.sharedInstance.updateTaskStatusById(taskID: taskId, isActive: false, isCompleted: true)
            }
        }
    }
    
    // MARK: - UNUserNotificationCenterDelegate
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completeTaskInNotification(notification: response.notification)
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completeTaskInNotification(notification: notification)
        completionHandler([.alert, .sound, .badge])
    }
    
}
