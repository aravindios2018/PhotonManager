//
//  Task.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 6/30/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import Foundation
import RealmSwift

/// Task model class
@objcMembers class Task: Object {
  
    // MARK: - Variables
    
    dynamic var name = ""
    dynamic var taskDescription = ""
    dynamic var isActive = false
    dynamic var isCompleted = false
    dynamic var dueDateInterval: Double = 0
    dynamic var taskID = UUID().uuidString
    dynamic var timestamp = Date().timeIntervalSince1970
    
    let tags = List<String>()
    
    override static func primaryKey() -> String? {
        return "taskID"
    }
    
}
